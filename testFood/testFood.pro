#-------------------------------------------------
#
# Project created by QtCreator 2014-09-03T22:00:27
#
#-------------------------------------------------

QT       += sql webkit testlib

TARGET = tst_testfoodtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_testfoodtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
