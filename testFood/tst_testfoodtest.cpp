#include <QtGui>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

class TestFoodTest : public QObject
{
    Q_OBJECT
    
public:
    TestFoodTest();
    
private Q_SLOTS:
    void testGui();
    void testGui_data();
};

TestFoodTest::TestFoodTest()
{
}

void TestFoodTest::testGui()
{
    QFETCH(QTestEventList, events);
    QFETCH(QString, expected);

    QLineEdit lineEdit;
    QSpinBox spinBox;

    events.simulate(&lineEdit);
    events.simulate(&spinBox);

    QCOMPARE(lineEdit.text(), expected);
    QCOMPARE(spinBox.text(), expected);
}

void TestFoodTest::testGui_data()
{
    QTest::addColumn<QTestEventList>("events");
    QTest::addColumn<QString>("expected");

    QTestEventList list1;
    list1.addKeyClick('a');
    QTest::newRow("char") << list1 << "a";

    QTestEventList list2;
    list2.addKeyClick('a');
    list2.addKeyClick(Qt::Key_Backspace);
    QTest::newRow("there and back again") << list2 << "";

    QTestEventList list3;
    list3.addKeyClick('1');
    QTest::newRow("num") << list3 << "1";
}

QTEST_MAIN(TestFoodTest)

#include "tst_testfoodtest.moc"
