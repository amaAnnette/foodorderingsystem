#include "admindashboard.h"
#include "ui_admindashboard.h"
#include "Jzon.h"
#include "iostream"
#include "mapview.h"
#include <QMessageBox>

using namespace std;

adminDashboard::adminDashboard(QWidget *parent) : QMainWindow(parent),
    adminUi(new Ui::adminDashboard)
{
    adminUi->setupUi(this);
    adminUi->widgetAddRestaurant->hide();
    adminUi->widgetHelp->hide();
    adminUi->widgetOrders->hide();
    adminUi->widgetRestaurantListings->hide();

    adminConn = new DataConnection;
    if(adminConn->openConnection()){
        adminUi->labelAdminStatus->setText("[+]Connection was successful :)");
        adminUi->labelAdminStatus->setStyleSheet("color:rgb(0, 255, 0)");
    } else{
        adminUi->labelAdminStatus->setText("[!]Database file does not exist ;(");
        adminUi->labelAdminStatus->setStyleSheet("color:rgb(255, 0, 0)");
    }
}

adminDashboard::~adminDashboard()
{
    delete adminUi;
    adminConn->closeConnection();
    delete adminConn;
    delete model;
}

void adminDashboard::addRestaurant(QString name, QString location, QString geocode)
{
    if(!adminConn->openConnection()){
        qDebug() << "Not connected ;(";
        return;
    }

    QSqlQuery qry;
    qry.prepare("INSERT INTO restaurant(name, location, geocodeID) VALUES('" + name + "', '" + location + "', '"+ geocode +"')");

    if(qry.exec()){
        QMessageBox::information(this, tr("Status"), tr("Data was successfully saved!"));
        adminConn->closeConnection();
    } else{
        QMessageBox::critical(this, tr("Status"), qry.lastError().text());
    }
}

void adminDashboard::addStaff(QString restName, QString restPassword)
{
    if(!adminConn->openConnection()){
        qDebug() << "Not connected ;(";
        return;
    }

    QSqlQuery qry;
    qry.prepare("INSERT INTO user(email, password) VALUES('" + restName + "@rest.com', '" + restPassword + "')");

    if(qry.exec()){
        qDebug() << "Added restaurant staff";
        adminConn->closeConnection();
    } else{
        qDebug() << "An error occured";
    }
}

void adminDashboard::addMenu(QString title, QString recipe, QString price, QString restID)
{
    if(!adminConn->openConnection()){
        qDebug() << "Not connected ;(";
        return;
    }

    QSqlQuery qry;
    qry.prepare("INSERT INTO menu(title, recipe, price, restID) VALUES('" + title + "', '" + recipe + "', '"+ price +"', '"+ restID +"')");

    if(qry.exec()){
        QMessageBox::information(this, tr("Status"), tr("Data was successfully saved!"));
        adminConn->closeConnection();
    } else{
        QMessageBox::critical(this, tr("Status"), qry.lastError().text());
    }
}


void adminDashboard::listings(QString customQuery){
    model = new QSqlQueryModel();
    adminConn->openConnection();
    QSqlQuery *qry = new QSqlQuery(adminConn->dbase);
    qry->prepare(customQuery);
    qry->exec();
    model->setQuery(*qry);
}


void adminDashboard::on_actionAdd_restaurant_triggered()
{
    adminUi->widgetAddRestaurant->show();
    adminUi->widgetHelp->hide();
    adminUi->widgetOrders->hide();
    adminUi->widgetRestaurantListings->hide();

    QString query = "SELECT name, id FROM restaurant";
    listings(query);
    adminUi->cmbMenuItemRest->setModel(model);
    adminConn->closeConnection();

    QString query2 = "SELECT placename FROM geocodes";
    listings(query2);
    adminUi->cmbLocation->setModel(model);
    adminConn->closeConnection();
}


void adminDashboard::on_actionOrders_triggered()
{
    adminUi->widgetAddRestaurant->hide();
    adminUi->widgetHelp->hide();
    adminUi->widgetOrders->show();
    adminUi->widgetRestaurantListings->hide();

    QString query = "SELECT name FROM restaurant";
    listings(query);
    adminUi->comboBoxRest->setModel(model);
    adminConn->closeConnection();
}


void adminDashboard::on_actionRestaurants_triggered()
{
    adminUi->widgetAddRestaurant->hide();
    adminUi->widgetHelp->hide();
    adminUi->widgetOrders->hide();
    adminUi->widgetRestaurantListings->show();

    QString query = "SELECT name, location FROM restaurant";
    listings(query);
    adminUi->tableRestaurantLists->setModel(model);
    adminConn->closeConnection();
}


void adminDashboard::on_actionMap_triggered()
{
    mapview viewMaps;
    viewMaps.setModal(true);
    viewMaps.exec();
}


void adminDashboard::on_actionHelp_triggered()
{
    adminUi->widgetAddRestaurant->hide();
    adminUi->widgetHelp->show();
    adminUi->widgetOrders->hide();
    adminUi->widgetRestaurantListings->hide();
}


void adminDashboard::on_buttonClearRest_clicked()
{
    adminUi->txtRestaurantName->clear();
    adminUi->cmbLocation->setCurrentIndex(0);
}


void adminDashboard::on_buttonAddRest_clicked()
{
    QString restName, geo, restname;
    restName = adminUi->txtRestaurantName->text().toUpper();
    geo = adminUi->cmbLocation->currentText();
    restname = restName.toLower().replace(" ", "_");
    QString geocodeID = "";

    adminConn->openConnection();
    QSqlQuery qery;
    qery.prepare("SELECT id FROM geocodes WHERE placename = '"+ geo +"'");
    if(qery.exec()){
        while(qery.next()){
            geocodeID = qery.value(0).toString();
        }
        adminConn->closeConnection();
    }

    addRestaurant(restName, geo, geocodeID);
    addStaff(restname, restname);
    on_buttonClearRest_clicked();
    on_actionAdd_restaurant_triggered();
}


void adminDashboard::on_groupBoxRestDetails_clicked()
{
    adminUi->groupBoxMenuAdd->setEnabled(false);
    adminUi->groupBoxRestDetails->setEnabled(true);
}


void adminDashboard::on_groupBoxMenuAdd_clicked()
{
    adminUi->groupBoxMenuAdd->setEnabled(true);
    adminUi->groupBoxRestDetails->setEnabled(true);
}


void adminDashboard::on_buttonAddMenu_clicked()
{
    QString title, recipe, prize;
    QString restid = on_cmbMenuItemRest_currentIndexChanged();;
    title = adminUi->txtMenuItem->text();
    recipe = adminUi->txtRecipe->text();
    prize = adminUi->dspPrice->text();

    addMenu(title, recipe, prize, restid);
}


void adminDashboard::on_tableRestaurantLists_activated(const QModelIndex &index)
{
    QString val = adminUi->tableRestaurantLists->model()->data(index).toString();

    QString query;
    query = "SELECT title, recipe, price FROM  menu WHERE menu.restID = ";
    query += "(SELECT id FROM restaurant WHERE name = '"+ val +"' ";
    query += "OR location = '"+ val +"')";

    listings(query);
    adminUi->tableMenLists->setModel(model);
    adminConn->closeConnection();
}


QString adminDashboard::on_cmbMenuItemRest_currentIndexChanged()
{
    QString name = adminUi->cmbMenuItemRest->currentText();
    QString result = "";

    if(!adminConn->openConnection()){
        qDebug() << "Not connected ;(";
    }

    QSqlQuery qry;
    qry.prepare("SELECT id FROM restaurant where name = '"+ name +"'");
    if(qry.exec()){
        while(qry.next()){
            result = qry.value(0).toString();
            return result;
        }
    } else{
        qDebug() << "Query was wrong ;(";
    }

}


void adminDashboard::on_buttonBrowse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Get recipe files"), "/home/kizito/", tr("Text file (*.txt)"));
    adminUi->txtRecipe->setText(fileName);
}


void adminDashboard::on_buttonClearMenu_clicked()
{
    adminUi->txtMenuItem->clear();
    adminUi->txtRecipe->clear();
    adminUi->dspPrice->clear();
}


QString adminDashboard::on_comboBoxRest_currentIndexChanged()
{
    QString name = adminUi->comboBoxRest->currentText();
    QString result = "";

    if(!adminConn->openConnection()){
        qDebug() << "Not connected ;(";
    }

    QSqlQuery qry;
    qry.prepare("SELECT id FROM restaurant where name = '"+ name +"'");
    if(qry.exec()){
        while(qry.next()){
            result = qry.value(0).toString();
            return result;
        }
    } else{
        qDebug() << "Query was wrong ;(";
    }
}


void adminDashboard::on_btnViewOrders_clicked()
{
    QString restaurantID, query;
    restaurantID = on_comboBoxRest_currentIndexChanged();
    query = "SELECT token AS 'Token', title as 'Menu item', quantity AS 'Quantity', ";
    query += "total AS 'Amount', customer_name AS 'Customer name', ";
    query += "address AS 'Customer address',phone_number AS 'Customer phone', ";
    query += "timestamp FROM menu, orders ";
    query += "WHERE orders.restaurantID = '"+ restaurantID +"' ";
    query += "AND orders.menuuID = menu.id ORDER BY orders.timestamp";

    listings(query);
    adminUi->tableOrders->setModel(model);
    adminConn->closeConnection();
}


void adminDashboard::on_buttonPushOrders_clicked()
{
    int row = adminUi->tableOrders->model()->rowCount();
    int col = adminUi->tableOrders->model()->columnCount();
    string restname = adminUi->comboBoxRest->currentText().toStdString();
    string save_as = restname + ".json";

    Jzon::Object root;
    root.Add("restaurant", restname);
    Jzon::Object results;

    string order_details = "";
    string tokenizer = "";
    for(int i = 0; i < row; i++){
        Jzon::Array orders;
        for(int j = 0; j < col; j++){
            if(j == 0){
                tokenizer = adminUi->tableOrders->model()->
                        data(adminUi->tableOrders->model()->index(i,j)).
                        toString().toStdString();
                continue;
            }
            order_details = adminUi->tableOrders->model()->
                    data(adminUi->tableOrders->model()->index(i,j)).
                    toString().toStdString();
            orders.Add(order_details);
        }
        results.Add(tokenizer, orders);

    }
    root.Add("orders", results);
    Jzon::FileWriter::WriteFile(save_as, root, Jzon::StandardFormat);
    QMessageBox::information(this, tr("Status"), tr("Orders have be pushed successfully!!!"));

}
