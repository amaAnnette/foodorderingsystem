#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dataconnection.h"
#include <QMenu>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void moduleLists(QString qry);
    void addOrder(QString restID, QString menuID, int qty, double total,
                   QString phone, QString token, QString timestamp);
    QString determine_id_from_combo(QString val, QString tableUsed, QString column);
    int determine_token();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void customMenuRequested(const QPoint& pos);

private slots:
    void on_actionRestaurants_triggered();
    void on_actionUser_actions_triggered();
    void on_actionExit_triggered();
    void on_actionAdministrator_triggered();
    void on_actionOrder_triggered();
    void on_actionAbout_triggered();
    void on_cmbOrderMenu_currentIndexChanged(const QString &arg1);
    void on_cmbOrderRest_currentIndexChanged(const QString &arg1);
    void on_btnOrderNext_clicked();
    void on_btnGoBack_clicked();
    void on_btnOrder_clicked();
    QString on_cmbOrderRest_currentIndexChanged();
    QString on_cmbOrderMenu_currentIndexChanged();

    void on_actionRestaurant_Panel_triggered();

private:
    Ui::MainWindow *mainUi;
    DataConnection *userConn;
    QSqlQueryModel *model;
    QMenu *myMenu;
};

#endif // MAINWINDOW_H
