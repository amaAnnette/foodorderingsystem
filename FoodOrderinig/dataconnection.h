#ifndef DATACONNECTION_H
#define DATACONNECTION_H

#include <QObject>
#include <QDialog>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>

class DataConnection : public QObject
{
    Q_OBJECT

public:
    QSqlDatabase dbase;
    bool openConnection();
    void closeConnection();

public:
    explicit DataConnection(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // DATACONNECTION_H
