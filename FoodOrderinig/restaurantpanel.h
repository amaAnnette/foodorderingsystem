#ifndef RESTAURANTPANEL_H
#define RESTAURANTPANEL_H

#include <QMainWindow>
#include "dataconnection.h"
#include <QMMapView.h>
#include <QMCoordinateRegion.h>
#include <QMenu>
#include "mapview.h"
#include "ui_mapview.h"

using namespace std;

namespace Ui {
class restaurantPanel;
}

class restaurantPanel : public QMainWindow
{
    Q_OBJECT

public:
    Ui::restaurantPanel *ui;
    void myView(QString customQuery);
    QString getRestID(QString name);

public:
    explicit restaurantPanel(QWidget *parent = 0);
    ~restaurantPanel();

private slots:
    void on_actionOrders_triggered();
    void on_tableOrderView_activated(const QModelIndex &index);
    void on_btnBack_clicked();

private:
    DataConnection *restConn;
    QSqlQueryModel *model;
    QMMapView *newMap;
    QMenu *myMenu;
    //mapview *view_map;
};

#endif // RESTAURANTPANEL_H
