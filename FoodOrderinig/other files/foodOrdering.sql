###USER TABLE DOCUMENTATION####
#                             #
# role should be either admin #
# or user                     #
#                             #
###############################

drop table if exists user;
create table user(
	id integer primary key not null,
	email text unique not null,
	password text not null,
	address1 text not null,
	address2 text null,
	role text not null
);

drop table if exists restaurant;
create table restaurant(
	id integer primary key not null,
	name text not null,
	location text not null
);

drop table if exists menu;
create table menu(
	id integer primary key not null,
	title text not null,
	restaurant_id integer not null,
	recipe blob null,
	price real not null
);

drop table if exists orders;
create table orders(
	id integer primary key not null,
	menu_id integer not null,
	user_id integer not null,
	restaurant_id integer not null
);