#ifndef DIALOGLOGIN_H
#define DIALOGLOGIN_H

#include <QDialog>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include <QMessageBox>
#include "admindashboard.h"
#include "dataconnection.h"
#include <restaurantpanel.h>
#include <iostream>

namespace Ui {
class dialogLogin;
}

class dialogLogin : public QDialog
{
    Q_OBJECT

public:
    void adminLogin(QString email, QString password, QString role);
    Ui::dialogLogin *loginUi;

public:
    explicit dialogLogin(QWidget *parent = 0);
    ~dialogLogin();

    
private slots:
    void on_buttonAdminLogin_clicked();
    void on_buttonAdminCancel_clicked();

private:
    adminDashboard *adminWin;
    restaurantPanel *restWin;
    DataConnection *connection;
};

#endif // DIALOGLOGIN_H
