#include "mapview.h"
#include "ui_mapview.h"

mapview::mapview(QWidget *parent) : QDialog(parent), ui(new Ui::mapview)
{
    ui->setupUi(this);
    _mapView = new QMMapView(QMMapView::RoadMap,
                             QMCoordinate(46.4981, 11.3494),
                             16);
    ui->mainLayout->addWidget(_mapView, 1);
    connect(_mapView, SIGNAL(mapLoaded()), this, SLOT(onMapLoaded()));
    connect(ui->btnLocate, SIGNAL(clicked()), this, SLOT(locateRestaurant()));

    mapConn = new DataConnection;
    if(mapConn->openConnection()){
        qDebug() << "You are connected.";
    } else{
        qDebug() << "Connection failed.";
    }

    QString query2;
    query2 = "SELECT name FROM restaurant";
    locationListings(query2);
    ui->cmbRestLists->setModel(model);
    mapConn->closeConnection();
}

mapview::~mapview()
{
    delete ui;
    delete _mapView;
    delete newMap;
}

void mapview::onMapLoaded(){
    QString("Type %1 at (%2, %3) , zoom %4").arg(
                QString::number(_mapView->mapType()),
                QString::number(_mapView->center().latitude()),
                QString::number(_mapView->center().longitude()),
                QString::number(_mapView->zoomLevel()));
}

void mapview::locationListings(QString customQuery)
{
    model = new QSqlQueryModel();
    mapConn->openConnection();
    QSqlQuery *qry = new QSqlQuery(mapConn->dbase);
    qry->prepare(customQuery);
    qry->exec();
    model->setQuery(*qry);
}

void mapview::locateRestaurant()
{
    mapConn->openConnection();
    QString query, rest;
    qreal lat = 0.00, longi = 0.00;
    rest = ui->cmbRestLists->currentText();
    query = "SELECT latitude, longitude FROM geocodes ";
    query += "WHERE geocodes.id = ";
    query += "(SELECT geocodeID FROM restaurant WHERE name = '"+ rest +"')";
    QSqlQuery qry;
    qry.prepare(query);
    if(qry.exec()){
        while(qry.next()){
            lat = qry.value(0).toReal();
            longi = qry.value(1).toReal();
        }
        mapConn->closeConnection();
    }

    delete _mapView;
    delete newMap;
    newMap = new QMMapView(QMMapView::RoadMap,
                             QMCoordinate(lat, longi),
                             16);
    ui->mainLayout->addWidget(newMap, 1);
}
