#include "dialoglogin.h"
#include "ui_dialoglogin.h"
#include <ui_restaurantpanel.h>
#include <iostream>

dialogLogin::dialogLogin(QWidget *parent) : QDialog(parent),
    loginUi(new Ui::dialogLogin){
    loginUi->setupUi(this);

    connection = new DataConnection;

    if(connection->openConnection()){
        loginUi->labelLoginStatus->setText("[+]Connection was successful :)");
        loginUi->labelLoginStatus->setStyleSheet("color:rgb(0, 255, 0)");
    } else{
        loginUi->labelLoginStatus->setText("[!]Database file does not exist ;(");
        loginUi->labelLoginStatus->setStyleSheet("color:rgb(255, 0, 0)");
    }
}

dialogLogin::~dialogLogin(){
    delete loginUi;
    connection->closeConnection();
    delete connection;
}

void dialogLogin::adminLogin(QString email, QString password, QString role){
    if(!connection->openConnection()){
        qDebug() << "Not connected ;(";
        return;
    }

    QSqlQuery qry;
    qry.prepare("SELECT email, password, role FROM user WHERE email=\'" + email +
                "\'AND password=\'" + password + "\'AND role=\'"+ role +"\'");

    if(qry.exec()){
        if(qry.next()){
            loginUi->labelLoginStatus->setText("[+]Valid email and password :)");
            loginUi->labelLoginStatus->setStyleSheet("color:rgb(0, 255, 0)");
            connection->closeConnection();
            this->close();
            if(role == "admin"){
                adminWin = new adminDashboard;
                adminWin->show();
            } else if(role == "staff"){
                QString restName = "", temp = "";
                QStringList pieces = loginUi->lineAdminEmail->text().split("@");
                temp = pieces.takeFirst();
                restName = temp.replace("_", " ").toUpper();

                restWin = new restaurantPanel;
                restWin->ui->lineRest->setText(restName);
                restWin->show();
            }

        } else{
            loginUi->labelLoginStatus->setText("[-]Wrong email and password :(");
            loginUi->labelLoginStatus->setStyleSheet("color:rgb(255, 0, 0)");
        }
    }
}

void dialogLogin::on_buttonAdminLogin_clicked(){
    QString email, password, role;
    email = loginUi->lineAdminEmail->text();
    password = loginUi->lineAdminPassword->text();
    role = loginUi->cmbRole->currentText();


    adminLogin(email, password, role);
}

void dialogLogin::on_buttonAdminCancel_clicked(){
    close();
}
