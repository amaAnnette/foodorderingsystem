#-------------------------------------------------
#
# Project created by QtCreator 2014-07-15T23:00:12
#
#-------------------------------------------------

QT       += core gui sql webkit

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FoodOrderinig
TEMPLATE = app

INCLUDEPATH += ../src
LIBS += -L../lib -lqtmapkit


SOURCES += main.cpp\
        mainwindow.cpp \
    dialoglogin.cpp \
    admindashboard.cpp \
    dialogabout.cpp \
    dataconnection.cpp \
    Jzon.cpp \
    mapview.cpp \
    restaurantpanel.cpp

HEADERS  += mainwindow.h \
    dialoglogin.h \
    admindashboard.h \
    dialogabout.h \
    dataconnection.h \
    Jzon.h \
    mapview.h \
    restaurantpanel.h

FORMS    += mainwindow.ui \
    dialoglogin.ui \
    admindashboard.ui \
    dialogabout.ui \
    mapview.ui \
    restaurantpanel.ui

RESOURCES += \
    resource.qrc

OTHER_FILES +=
