#ifndef ADMINDASHBOARD_H
#define ADMINDASHBOARD_H

#include <QMainWindow>
#include <QMessageBox>
#include "dataconnection.h"
#include <QFileDialog>

namespace Ui {
class adminDashboard;
}

class adminDashboard : public QMainWindow
{
    Q_OBJECT

public:
    void addRestaurant(QString name, QString location, QString geoCode);
    void addMenu(QString title, QString recipe, QString price, QString restID);
    void listings(QString qry);
    void addStaff(QString restName, QString restPassword );
    
public:
    explicit adminDashboard(QWidget *parent = 0);
    ~adminDashboard();
    
private slots:
    void on_actionAdd_restaurant_triggered();
    void on_actionOrders_triggered();
    void on_actionRestaurants_triggered();
    void on_actionMap_triggered();
    void on_actionHelp_triggered();
    void on_buttonClearRest_clicked();
    void on_buttonAddRest_clicked();
    void on_groupBoxRestDetails_clicked();
    void on_groupBoxMenuAdd_clicked();
    void on_buttonAddMenu_clicked();
    void on_tableRestaurantLists_activated(const QModelIndex &index);
    QString on_cmbMenuItemRest_currentIndexChanged();
    void on_buttonBrowse_clicked();
    void on_buttonClearMenu_clicked();
    QString on_comboBoxRest_currentIndexChanged();
    void on_btnViewOrders_clicked();
    void on_buttonPushOrders_clicked();

private:
    Ui::adminDashboard *adminUi;
    DataConnection *adminConn;
    QSqlQueryModel *model;
};

#endif // ADMINDASHBOARD_H
