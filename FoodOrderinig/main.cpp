#include <QtGui>

#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>
#include <QTimer>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle(new QPlastiqueStyle);

    QFile file(":/qss/customStyle.qss");
    file.open(QFile::ReadOnly);
    a.setStyleSheet(file.readAll());


    QSplashScreen *splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/images/restaurant-icon.png"));
    splash->show();

    MainWindow w;

    QTimer::singleShot(2500, splash, SLOT(close()));
    QTimer::singleShot(2500, &w, SLOT(show()));

    //w.show();

    return a.exec();
}
