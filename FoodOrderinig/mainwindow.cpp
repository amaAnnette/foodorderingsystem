/*
 * what the main users will see.
 * main users are not suppose to be logged in before they can order
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialoglogin.h"
#include "ui_dialoglogin.h"
#include "dialogabout.h"
#include <Jzon.h>
#include <QDateTime>
#include "mapview.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

//my debugger
using namespace std;

//constructor
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    mainUi(new Ui::MainWindow)
{
    mainUi->setupUi(this);
    mainUi->widgetRestList->show();
    mainUi->widgetOrder->hide();

    //set a new instance of database connection
    //...which will be used throughout this class
    userConn = new DataConnection;
    if(userConn->openConnection()){
        mainUi->labelStatus->setText("[+]Connection was successful :)");
        mainUi->labelStatus->setStyleSheet("color:rgb(0, 255, 0)");
    } else{
        mainUi->labelStatus->setText("[!]Database file does not exist ;(");
        mainUi->labelStatus->setStyleSheet("color:rgb(255, 0, 0)");
    }

    //custom context menu slot and signal declaration
    mainUi->tableRestaurants->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(mainUi->tableRestaurants, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(customMenuRequested(const QPoint&)));

    mainUi->actionRestaurants->trigger();
}

//destructor
MainWindow::~MainWindow()
{
    //delete instance of class pointer and any other pointer
    //...set within this instance here.
    delete mainUi;
    userConn->closeConnection();
    delete userConn;
    delete model;
}

/*
 * custom functions to boost functionality ...
 */

//custom context menu
void MainWindow::customMenuRequested(const QPoint& pos)
{
    /*
     *provide a context menu on demand(by right clicking on an object
     *specified by the pos variable.
     */
    QPoint globalPos = mainUi->tableRestaurants->mapToGlobal(pos);
    myMenu = new QMenu(this);
    myMenu->addAction("View map");
    QAction* selectedItem = myMenu->exec(globalPos);
    if (selectedItem){
        mapview view_map;
        view_map.setModal(true);
        view_map.exec();
    }
}

//token generator
int MainWindow::determine_token()
{
   /*
    *a token generator based on random generator concept with .c's
    *srand and current system time as seeder
    */
   srand((unsigned)time(0));
   int token = rand();
   return token;
}

//moduleLists
void MainWindow::moduleLists(QString customQuery)
{
    /*
     * a database function that lists out query results based on query passed
     * and can be used to populate listviews, tableviews and comboboxes
     */
    model = new QSqlQueryModel();
    userConn->openConnection();
    QSqlQuery *qry = new QSqlQuery(userConn->dbase);
    qry->prepare(customQuery);
    qry->exec();
    model->setQuery(*qry);
}

QString MainWindow::on_cmbOrderRest_currentIndexChanged()
{
    QString name = mainUi->cmbOrderRest->currentText();
    QString result = "";

    if(!userConn->openConnection()){
        qDebug() << "Not connected ;(";
    }

    QSqlQuery qry;
    qry.prepare("SELECT id FROM restaurant where name = '"+ name +"'");
    if(qry.exec()){
        while(qry.next()){
            result = qry.value(0).toString();
            return result;
        }
    } else{
        qDebug() << "Query was wrong ;(";
    }
}

QString MainWindow::on_cmbOrderMenu_currentIndexChanged()
{
    QString title = mainUi->cmbOrderMenu->currentText();
    QString result = "";

    if(!userConn->openConnection()){
        qDebug() << "Not connected ;(";
    }

    QSqlQuery qry;
    qry.prepare("SELECT id FROM menu where title = '"+ title +"'");
    if(qry.exec()){
        while(qry.next()){
            result = qry.value(0).toString();
            return result;
        }
    } else{
        qDebug() << "Query was wrong ;(";
    }
}

/*
 * end of custom functions ...
 */


/*
 * private slots ...
 */
void MainWindow::on_actionRestaurants_triggered()
{
    mainUi->widgetRestList->show();
    mainUi->widgetOrder->hide();

    QString query = "SELECT name, location FROM restaurant";
    moduleLists(query);
    mainUi->tableRestaurants->setModel(model);
    userConn->closeConnection();
}


void MainWindow::on_actionUser_actions_triggered()
{
    mainUi->widgetRestList->hide();
    mainUi->widgetOrder->hide();
}


void MainWindow::on_actionExit_triggered()
{
    close();
}


void MainWindow::on_actionAdministrator_triggered()
{
    //initiate the login window for admin ...
    dialogLogin loginWin;
    loginWin.loginUi->cmbRole->setCurrentIndex(1);
    loginWin.setModal(true);
    loginWin.exec();
}

void MainWindow::on_actionOrder_triggered()
{
    mainUi->widgetRestList->hide();
    mainUi->widgetOrder->show();

    QString query = "SELECT name FROM restaurant";
    moduleLists(query);
    mainUi->cmbOrderRest->setModel(model);
    userConn->closeConnection();

    query = "SELECT placename FROM geocodes";
    moduleLists(query);
    mainUi->cmbOrderAddress->setModel(model);
    userConn->closeConnection();

    mainUi->orderDelivery->setEnabled(false);
    mainUi->orderStart->setEnabled(true);
}


void MainWindow::on_actionAbout_triggered()
{
    //instantiate the about dialogbox ...
    dialogAbout aboutWin;
    aboutWin.setModal(true);
    aboutWin.exec();
}

void MainWindow::on_cmbOrderMenu_currentIndexChanged(const QString &arg1)
{
    QString ordMenuName = mainUi->cmbOrderMenu->currentText();

    userConn->openConnection();
    QSqlQuery qry;
    qry.prepare("SELECT price FROM menu WHERE title = '"+ ordMenuName +"'");

    if(qry.exec()){
        while(qry.next()){
            mainUi->txtOrderPrice->setText(qry.value(0).toString());
        }
        userConn->closeConnection();
    } else{
        qDebug()<<"didnt work";
    }
}


void MainWindow::on_cmbOrderRest_currentIndexChanged(const QString &arg1)
{
    QString ordRestName = mainUi->cmbOrderRest->currentText();
    QString query = "SELECT title FROM  menu WHERE menu.restID = (SELECT id FROM restaurant WHERE name = '"+ ordRestName +"')";
    moduleLists(query);
    mainUi->cmbOrderMenu->setModel(model);
    userConn->closeConnection();
}


void MainWindow::on_btnOrderNext_clicked()
{
    mainUi->orderDelivery->setEnabled(true);
    mainUi->orderStart->setEnabled(false);
}


void MainWindow::on_btnGoBack_clicked()
{
    mainUi->orderStart->setEnabled(true);
    mainUi->orderDelivery->setEnabled(false);
}


//order
void MainWindow::on_btnOrder_clicked()
{ 
    if(!userConn->openConnection()){
        qDebug() << "Not connected ;(";
    }

    userConn->openConnection();
    QString restID, menuID, qty, price, timestamp,
            address, phone_number, customer_name;
    QDateTime datetime;
    QString token;

    address = mainUi->cmbOrderAddress->currentText();
    phone_number = mainUi->txtOrderPhone->text();
    customer_name = mainUi->txtOrderFName->text() + " "
            + mainUi->txtOrderSName->text();
    price = mainUi->txtOrderPrice->text();
    restID = on_cmbOrderRest_currentIndexChanged();
    menuID = on_cmbOrderMenu_currentIndexChanged();
    qty = mainUi->spinBoxQty->text();
    double totalAmount = qty.toInt() * price.toDouble();
    cout << totalAmount << endl;
    QString total;
    total = QString::number(totalAmount);
    token = (mainUi->cmbOrderRest->currentText() + "%1").arg(determine_token());
    datetime = QDateTime::currentDateTime();
    timestamp = datetime.toUTC().toString();

    QSqlQuery qry;
    qry.prepare("INSERT INTO orders(restaurantID, menuuID, quantity, total, timestamp, token, customer_name, address, phone_number) VALUES('"+ restID +"','"+ menuID +"','"+ qty +"','"+ total +"','"+ timestamp +"','"+ token +"','"+ customer_name +"', '"+ address +"', '"+ phone_number +"')");

    if(qry.exec()){
        QMessageBox::information(this, tr("Order feedback"), ("Your token is " + token));
    } else{
        QMessageBox::critical(this, tr("Order feedback"), tr("Your order didnt pull through. Please try again!!!"));
    }
    userConn->closeConnection();

    mainUi->txtOrderFName->clear();
    mainUi->txtOrderPhone->clear();
    mainUi->txtOrderPrice->clear();
    mainUi->txtOrderSName->clear();
    mainUi->cmbOrderAddress->setCurrentIndex(0);
    mainUi->cmbOrderRest->setCurrentIndex(0);
    mainUi->cmbOrderMenu->clear();
}

void MainWindow::on_actionRestaurant_Panel_triggered()
{
    //initiate the login window for admin ...
    dialogLogin loginWin;
    loginWin.loginUi->cmbRole->setCurrentIndex(2);
    loginWin.setModal(true);
    loginWin.exec();
}
