#ifndef MAPVIEW_H
#define MAPVIEW_H

#include <QDialog>
#include <QMMapView.h>
#include <QMCoordinateRegion.h>
#include "dataconnection.h"

namespace Ui {
class mapview;
}

class mapview : public QDialog
{
    Q_OBJECT

public:
    void locationListings(QString qry);
    Ui::mapview *ui;

public:
    explicit mapview(QWidget *parent = 0);
    ~mapview();
    
private:
    QMMapView *_mapView;
    QMMapView *newMap;
    DataConnection *mapConn;
    QSqlQueryModel *model;

private slots:
    void onMapLoaded();
    void locateRestaurant();
};

#endif // MAPVIEW_H
