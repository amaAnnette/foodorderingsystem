#include "dataconnection.h"
#define PATH_TO_DBASE_FILE "/home/kizito/Projektz/C_C++/foodorderingsystem/FoodOrderinig/foodordering.sqlite"

DataConnection::DataConnection(QObject *parent):QObject(parent){
    openConnection();
}

bool DataConnection::openConnection(){
    dbase = QSqlDatabase::addDatabase("QSQLITE");
    dbase.setDatabaseName(PATH_TO_DBASE_FILE);
    QFileInfo checkFile(PATH_TO_DBASE_FILE);

    if(checkFile.isFile()){
        if(dbase.open()){
            qDebug() << "Connection was successful :)";
            return true;
        }
    } else{
        qDebug() << "Database file does not exist ;(";
        return false;
    }
}

void DataConnection::closeConnection(){
    dbase.close();
    dbase.removeDatabase(QSqlDatabase::defaultConnection);

}
