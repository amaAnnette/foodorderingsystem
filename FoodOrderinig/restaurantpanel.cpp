#include "restaurantpanel.h"
#include "ui_restaurantpanel.h"
#include "ui_mapview.h"
#include <Jzon.h>

#include <iostream>


restaurantPanel::restaurantPanel(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::restaurantPanel)
{
    ui->setupUi(this);
    restConn = new DataConnection;
    if(restConn->openConnection()){
        ui->labelStatus->setText("[+]Connection was successful :)");
        ui->labelStatus->setStyleSheet("color:rgb(0, 255, 0)");
    } else{
        ui->labelStatus->setText("[!]Database file does not exist ;(");
        ui->labelStatus->setStyleSheet("color:rgb(255, 0, 0)");
    }

    ui->actionOrders->trigger();
}

restaurantPanel::~restaurantPanel()
{
    delete ui;
    restConn->closeConnection();
    delete model;
}


void restaurantPanel::myView(QString customQuery)
{
    model = new QSqlQueryModel();
    restConn->openConnection();
    QSqlQuery *qry = new QSqlQuery(restConn->dbase);
    qry->prepare(customQuery);
    qry->exec();
    model->setQuery(*qry);
}

void restaurantPanel::on_actionOrders_triggered()
{
    ui->widgetRestOrders->show();
    ui->widgetUserDir->hide();

    QString restaurantID = "";
    restConn->openConnection();
    QSqlQuery qry;
    qry.prepare("SELECT id FROM restaurant WHERE name = '"+ ui->lineRest->text() +"'");
    if(qry.exec()){
        while(qry.next()){
            restaurantID = qry.value(0).toString();
        }
        restConn->closeConnection();
    } else{
        ui->labelStatus->setText("something is wrong with your connection");
        ui->labelStatus->setStyleSheet("color:rgb(255,0,0)");
    }

    std::cout << restaurantID.toStdString() << std::endl;
    QString query;
    query = "SELECT token AS Token, title as 'Menu item', quantity AS 'Quantity', ";
    query += "total AS 'Amount', customer_name AS 'Customer name', ";
    query += "address AS 'Customer address',phone_number AS 'Customer phone', ";
    query += "timestamp FROM menu, orders ";
    query += "WHERE orders.restaurantID = '"+ restaurantID +"' ";
    query += "AND orders.menuuID = menu.id ORDER BY orders.timestamp";

    myView(query);
    ui->tableOrderView->setModel(model);
    restConn->closeConnection();
}

void restaurantPanel::on_tableOrderView_activated(const QModelIndex &index)
{
    QString val = ui->tableOrderView->model()
            ->index(0,5,index).data().toString();
    qreal lat = 0.00, longi = 0.00;

    restConn->openConnection();
    QSqlQuery qry;
    qry.prepare("SELECT latitude, longitude FROM geocodes WHERE placename = '"+ val +"'");
    if(qry.exec()){
        while(qry.next()){
            lat = qry.value(0).toReal();
            longi = qry.value(1).toReal();
        }
        restConn->closeConnection();
    }

    ui->widgetRestOrders->hide();
    ui->widgetUserDir->show();
    delete newMap;
    newMap = new QMMapView(QMMapView::RoadMap,
                             QMCoordinate(lat, longi),
                             16);
    ui->mapLayout->addWidget(newMap, 1);

}

void restaurantPanel::on_btnBack_clicked()
{
    ui->widgetRestOrders->show();
    ui->widgetUserDir->hide();
}
